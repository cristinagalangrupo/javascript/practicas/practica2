/* *********************************** Declaro las variables ************************************ */
var str = 0; // almacena la frase 
var total = 0; // almacena el total de caracteres
var a = 0; // cuenta de aes
var e = 0;// cuenta de es
var ies = 0;// cuenta de ies
var o = 0;// cuenta de oes
var u = 0;// cuenta de es

/* ************************************ Leo la frase ************************************  */

str = prompt("Introduce una frase");

/* ************************************  Cuento las vocales ************************************  */
for (i = 0; i < str.length; i++) {
    if (str.charAt(i) == 'a' || str.charAt(i) == 'A' || str.charAt(i) == 'á' || str.charAt(i) == 'Á') {
        a++;
    } else if (str.charAt(i) == 'e' || str.charAt(i) == 'E' || str.charAt(i) == 'é' || str.charAt(i) == 'É') {
        e++;
    } else if (str.charAt(i) == 'i' || str.charAt(i) == 'I' || str.charAt(i) == 'í' || str.charAt(i) == 'Í') {
        ies++;
    } else if (str.charAt(i) == 'o' || str.charAt(i) == 'O' || str.charAt(i) == 'ó' || str.charAt(i) == 'Ó') {
        o++;
    } else if (str.charAt(i) == 'u' || str.charAt(i) == 'U' || str.charAt(i) == 'ú' || str.charAt(i) == 'Ú') {
        u++;
    }

}
/* ************************************  Cuento el total de caracteres ************************************  */

total = str.length;

/* ************************************ Muestro en patalla ************************************  */
document.write(str + "<br>");
document.write("Total de caracteres: " + total + "<br>" + "<br>");
document.write("Repeticiones de a: " + a + " La frecuencia con la que aparece la  letra a es de " + a / total + "<br>");
document.write("Repeticiones de e: " + e + " La frecuencia con la que aparece la  letra e es de " + e / total + "<br>");
document.write("Repeticiones de i: " + ies + " La frecuencia con la que aparece la  letra i es de " + ies / total + "<br>");
document.write("Repeticiones de o: " + o + " La frecuencia con la que aparece la  letra o es de " + o / total + "<br>");
document.write("Repeticiones de u: " + u + " La frecuencia con la que aparece la  letra u es de " + u / total);