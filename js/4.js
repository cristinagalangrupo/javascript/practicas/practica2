/* ************************************  Declaro las variables ************************************  */
var numeros = []; // almacena los números
var c = 0; // almacena cuántos números desea introducir el usuario
var negativos = 0; // suma de números positivos
var positivos = 0; // suma de números negativos


/* ************************************ Leo los datos ************************************ */
c = parseInt(prompt("¿Cuántos números desea introducir?"));
for (i = 0; i < c; i++) {
    numeros[i] = parseInt(prompt("Introduce el " + (i + 1) + "º."));
}


// Leo los números. Si el usuario no tuviera claro cuántos números va a introducir podría pedir los datos con confirm
/*
while(confirm("¿Desea introducir un número?")){
	numeros[c]=parseInt(prompt("Introduce un número"));
	c++;
}
*/

/* ************************************ Sumo ************************************ */ 
for (i = 0; i < numeros.length; i++) {
    if (numeros[i] < 0) {
        negativos += numeros[i];
    } else {
        positivos += numeros[i];
    }
}

console.log("Suma de positivos:" + positivos, " Suma de negativos: " + negativos);