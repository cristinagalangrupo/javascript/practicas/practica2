/* ************************************ Declaro las variables ************************************ */
var str=0; // contiene la frase

var cuentavocales=0; // cuenta de vocales
var cuentaconso=0; // cuenta de vocales

var coincideV=0;
var coincideC=0;

// Creo dos arrays con las vocales y consonantes 
// por si la cadena tuviera 
// SIGNOS DE PUNTUACIÓN, 
// SÍMBOLOS O 
// NÚMEROS.

var vocales=['a','e','i','o','u','A','E','I','O','U','á','é','í','ó','ú','Á','É','Í','Ó','Ú','ü','Ü'];
var consonantes=['b','c','d','f','g','h','j','k','l','m','n','ñ','p','q','r','s','t','v','w','x','y','z','B','C','D','F','G','H','J','K','L','M','N','Ñ','P','Q','R','S','T','V','W','X','Y','Z'];

/* ************************************ Leo la cadena de caracteres ************************************ */

str = prompt("Introduce una frase");


/* ************************************ Cuento las VOCALES ************************************ */

var elemento=0; // Contiene el carácter de la posición i

for(i=0;i<str.length;i++){
	
	elemento=str[i]; // guardo la letra del string en la posición i
	coincideV=vocales.includes(elemento); // compruebo si está incluida en vocales
	cuentavocales+=coincideV;
}


/* ************************************ Cuento las CONSONANTES ************************************ */

var elto=0;// Contiene el carácter de la posición i

for(i=0;i<str.length;i++){
	
	elto=str[i];// guardo la letra del string en la posición i
	coincideC=consonantes.includes(elto);// compruebo si está incluida en consonantes
	cuentaconso+=coincideC;
}


/* ************************************ MUESTRO EN PANTALLA ************************************ */

document.write("Total de vocales: " + cuentavocales+ "<br>");
document.write("Total de consonantes: " + cuentaconso);

// Ej. "Hola. Me llamo Cristina." Tiene que devolver 8 vocales y 11 consonantes

