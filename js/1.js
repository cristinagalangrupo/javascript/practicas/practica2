var numeros = [];
var pares = 0;
var mx = 0; // Guarda el máximo
var mi = 0; // Guarda el mínimo


/* *********************************** Función para leer los números *********************************** */



function leer() {
    for (i = 0; i < 10; i++)
        numeros[i] = parseInt(prompt("Introduce un número"));
}



/* *********************************** Calcular pares *********************************** */
function calcpares(arg) {
    for (i = 0; i < 10; i++) {
        if ((arg[i] % 2) == 0) {
            pares++;
        }
    }
    return pares;
}

/* *********************************** Calcular máximo *********************************** */


function maximo(arg) {
    mx = arg[0];
    for (i = 0; i < arg.length; i++) {
        if (mx < arg[i]) {
            mx = arg[i];
        }
    }
}

/* *********************************** Calcular mínimo *********************************** */

function minimo(arg){
	mi=arg[0];
	for(i=0;i<arg.length;i++){
		if (mi>arg[i]) {
			mi=arg[i];
		}
	}
}

leer();
maximo(numeros);
minimo(numeros);
pares = calcpares(numeros);

/* 
//otra forma para sacar máximo y el mínimo
maximo=Math.max.apply(null,numeros); // devuelve el máximo del array
minimo=Math.min.apply(null,numeros); // devuelve el mínimo del array
*/


/* *********************************** Muestro resultado en pantalla *********************************** */

document.write("El número de pares es: " + pares + "<br>");
document.write("El máximo es: " + mx + "<br>");
document.write("El mínimo es: "+mi);