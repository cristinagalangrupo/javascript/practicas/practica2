/* ************************************  Declaro las variables ************************************  */
var lista1 = []; // Guarda la lista con todos los números
var lista2 = []; // Guarda los dos números que hay que comprobar si están en la primera lista
var coincidencia = 0; // almacena el número de coincidencias

/* ************************************ Creo una función para leer números ************************************  */

function leer() {
	for (i = 0; i < 10; i++)
		lista1[i] = parseInt(prompt("Introduce un número")); // primer array con los datos
	
	for (i = 0; i < 2; i++)
		lista2[i] = parseInt(prompt("Introduce un número")); // segundo array para comprobar si están los datos contenidos en el primero
}



/* ************************************ Creo una función que compara los datos del primer array y del segundo.
Me devuelve el número de coincidencias ************************************ */

function comparar(arg1, arg2) {
	var cont=0;
	for(i=0;i<arg2.length;i++){
		var elemento=arg2[i]; // almaceno en elemento el valor de primer elemento de la segunda lista
		var coincide=arg1.includes(elemento); //Almaceno en coincide 1 si coincide el elemto en la lista1 (arg1)   // includes devuelve 0 si no hay coincidencia y 1 si la hay
		cont+=coincide;
	}
	return cont;
}



/* ************************************ Leo los arrays ************************************  */
leer(); 

coincidencia=comparar(lista1, lista2); // almaceno el número de coincidencias




/* ************************************ Para que estén los dos números en la lista debe haber 2 coincidencias ************************************  */

if (coincidencia<2) {
	document.write("No están entre los anteriores");
} else{
	document.write("Sí están entre los anteriores");
}